#!/bin/bash

cd "${0%/*}"

name="repackage"
version="1.0.1"
description="Linux command line tool to convert packages from the AppImage and snap formats to the Arch, Debian, and RPM package formats"
sha256sum="$(sha256sum "$name".sh | sed 's/\ .*//')"

mkdir "$name"
  
cp "$name".sh "$name"

echo "pkgname=\"$name\"
pkgver=\"$version\"
arch=(\"any\")
pkgdesc=\""$description"\"
pkgrel=\"1\"
source=(\"file://./"$name".sh\")
sha256sums=(\""$sha256sum"\")

package() {
	install -d \"\${pkgdir}\"/usr/bin
	cp \"\${srcdir}\"/"$name".sh \"\${pkgdir}\"/usr/bin/"$name"

	chmod +x \"\${pkgdir}\"/usr/bin/"$name"
}" | tee "$name"/PKGBUILD