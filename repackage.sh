#!/bin/bash

Convert_Snap() {
	wget -O "$snap_id".snap https://api.snapcraft.io/api/v1/snaps/download/"$snap_id".snap
	unsquashfs -q -f -d source "$snap_id".snap

	name="$(cat source/meta/snap.yaml | grep name | sed 's/.*\:\ //' 
	$name)"
	version="$(cat source/meta/snap.yaml | grep version | sed 's/.*\:\ //')"
	description="$(cat source/meta/snap.yaml | grep description | sed 's/.*\:\ //')"
	sha256sum="$(sha256sum "$snap_id".snap | sed 's/\ .*//')"

	mkdir "$name"
}

Convert_AppImage() {
	cp "$appimage_file" /tmp

	chmod +x ./"${appimage_file##*/}"

	./"${appimage_file##*/}" --appimage-extract

	if [[ -f squashfs-root/default.desktop ]]; then
		name="$(cat squashfs-root/default.desktop | grep Name | sed 's/Name\=//')"
		icon="$(cat squashfs-root/default.desktop | grep Icon | sed 's/Icon\=//')"
	else
		name="$(find squashfs-root -name *.desktop | sed 's/squashfs-root\///' | sed 's/\.desktop//')"
		icon="$(find squashfs-root -name *.desktop | sed 's/squashfs-root\///' | sed 's/\.desktop//')"
	fi

	version="1.0"
	description="$name"
	sha256sum="$(sha256sum "${appimage_file##*/}" | sed 's/\ .*//')"
}

Convert_Snap_RPM() {
	Convert_Snap
	Create_RPM

	echo "squashfs -q -f -d /tmp/source /tmp/"$snap_id".snap
}

package() {
	install -d \$RPM_BUILD_ROOT/opt/"$name"
	cp -r /tmp/source/. \$RPM_BUILD_ROOT/opt/"$name"
 
	sed -i 's|\\\${SNAP}/meta/gui/icon.png|"$name"|g' \$RPM_BUILD_ROOT/opt/"$name"/meta/gui/"$name".desktop
	install -Dm644 \$RPM_BUILD_ROOT/opt/"$name"/meta/gui/"$name".desktop -t \$RPM_BUILD_ROOT/usr/share/applications
	install -Dm644 \$RPM_BUILD_ROOT/opt/"$name"/meta/gui/icon.png \$RPM_BUILD_ROOT/usr/share/pixmaps/"$name".png
	
	rm -rf \$RPM_BUILD_ROOT/opt/"$name"/{data-dir,gnome-platform,lib,meta,scripts,usr,*.sh}

	install -d \$RPM_BUILD_ROOT/usr/bin
	ln -s /opt/"$name"/"$name" \$RPM_BUILD_ROOT/usr/bin
}" | tee -a rpmbuild/SPECS/"$name".spec

	Build_RPM

	rm -r /tmp/squashfs-root /tmp/"$snap_id".snap
}

Convert_Snap_Debian() {
	Convert_Snap
	Create_Debian
	
	cp -r source/. $name/opt/$name

	sed -i "s|\${\SNAP}/meta/gui/icon.png|$name|g" $name/opt/$name/meta/gui/$name.desktop
	install -Dm644 $name/opt/$name/meta/gui/$name.desktop -t $name/usr/share/applications
	install -Dm644 $name/opt/$name/meta/gui/icon.png $name/usr/share/pixmaps/$name.png

	rm -rf $name/opt/$name/{data-dir,gnome-platform,lib,meta,scripts,usr,*.sh}

	Build_Debian

	rm -r "$snap_id".snap source $name
}

Convert_Snap_Arch() {
	Convert_Snap
	Create_Arch
	
	echo "unsquashfs -q -f -d \"\${srcdir}\"/source "$snap_id".snap
}

package() {
	install -d \"\${pkgdir}\"/opt/"$name"
	cp -r \"\${srcdir}\"/source/. \"\${pkgdir}\"/opt/"$name"
 
	sed -i 's|\\\${SNAP}/meta/gui/icon.png|"$name"|g' \"\${pkgdir}\"/opt/"$name"/meta/gui/"$name".desktop
	install -Dm644 \"\${pkgdir}\"/opt/"$name"/meta/gui/"$name".desktop -t \"\${pkgdir}\"/usr/share/applications
	install -Dm644 \"\${pkgdir}\"/opt/"$name"/meta/gui/icon.png \"\${pkgdir}\"/usr/share/pixmaps/"$name".png
	
	rm -rf \"\${pkgdir}\"/opt/"$name"/{data-dir,gnome-platform,lib,meta,scripts,usr,*.sh}

	install -d \"\${pkgdir}\"/usr/bin
	ln -s /opt/"$name"/"$name" \"\${pkgdir}\"/usr/bin
}" | tee -a "$name"/PKGBUILD

	Build_Arch

	rm -r source
}

Convert_AppImage_RPM() {
	Convert_AppImage
	Create_RPM

	echo "chmod +x /tmp/"${appimage_file##*/}"
	/tmp/"${appimage_file##*/}" --appimage-extract

	install -d \$RPM_BUILD_ROOT/opt/"$name"
	cp -r /tmp/squashfs-root/. \$RPM_BUILD_ROOT/opt/"$name"

	if [[ -f \$RPM_BUILD_ROOT/opt/"$name"/default.desktop ]]; then
		mv \$RPM_BUILD_ROOT/opt/"$name"/default.desktop \$RPM_BUILD_ROOT/opt/"$name"/"$name".desktop
	fi

	sed -i \"s|AppRun|/opt/"$name"/"$name"|\" \$RPM_BUILD_ROOT/opt/"$name"/"$name".desktop
	sed -i 's/Terminal\=true/Terminal\=false/' \$RPM_BUILD_ROOT/opt/"$name"/"$name".desktop
	install -Dm644 \$RPM_BUILD_ROOT/opt/"$name"/"$name".desktop -t \$RPM_BUILD_ROOT/usr/share/applications

	if [[ -f \$RPM_BUILD_ROOT/opt/"$name"/"$icon".png ]]; then
		install -Dm644 \$RPM_BUILD_ROOT/opt/"$name"/"$icon".png \$RPM_BUILD_ROOT/usr/share/pixmaps/"$icon".png
	else
		install -Dm644 \$RPM_BUILD_ROOT/usr/share/icons/hicolor/0x0/apps/"$icon".png \$RPM_BUILD_ROOT/usr/share/pixmaps/"$icon".png
	fi
	
	install -d \$RPM_BUILD_ROOT/usr/bin
	ln -s /opt/"$name"/"$name" \$RPM_BUILD_ROOT/usr/bin
	chmod -R 755 \$RPM_BUILD_ROOT/opt/"$name"
}" | tee -a rpmbuild/SPECS/"$name".spec

	Build_RPM
}

Convert_AppImage_Debian() {
	Convert_AppImage
	Create_Debian

	cp -r squashfs-root/. $name/opt/$name

	mv $name/opt/$name/default.desktop $name/opt/$name/$name.desktop

	sed -i "s|AppRun|/opt/$name/$name|" $name/opt/$name/$name.desktop
	sed -i 's/Terminal\=true/Terminal\=false/' $name/opt/$name/$name.desktop
	install -Dm644 $name/opt/$name/$name.desktop -t $name/usr/share/applications
	install -Dm644 $name/opt/$name/"$icon".png $name/usr/share/pixmaps/"$icon".png

	Build_Debian

	rm -r "${appimage_file##*/}" squashfs-root $name
}

Convert_AppImage_Arch() {
	Convert_AppImage
	Create_Arch

	echo "chmod +x \"\${srcdir}\"/"${appimage_file##*/}"
	\"\${srcdir}\"/"${appimage_file##*/}" --appimage-extract
}

package() {
	install -d \"\${pkgdir}\"/opt/"$name"
	cp -r \"\${srcdir}\"/squashfs-root/. \"\${pkgdir}\"/opt/"$name"

	if [[ -f \"\${pkgdir}\"/opt/"$name"/default.desktop ]]; then
		mv \"\${pkgdir}\"/opt/"$name"/default.desktop \"\${pkgdir}\"/opt/"$name"/"$name".desktop
	fi

	sed -i \"s|AppRun|/opt/"$name"/"$name"|\" \"\${pkgdir}\"/opt/"$name"/"$name".desktop
	sed -i 's/Terminal\=true/Terminal\=false/' \"\${pkgdir}\"/opt/"$name"/"$name".desktop
	install -Dm644 \"\${pkgdir}\"/opt/"$name"/"$name".desktop -t \"\${pkgdir}\"/usr/share/applications

	if [[ -f \"\${pkgdir}\"/opt/"$name"/"$icon".png ]]; then
		install -Dm644 \"\${pkgdir}\"/opt/"$name"/"$icon".png \"\${pkgdir}\"/usr/share/pixmaps/"$icon".png
	else
		install -Dm644 \"\${pkgdir}\"/usr/share/icons/hicolor/0x0/apps/"$icon".png \"\${pkgdir}\"/usr/share/pixmaps/"$icon".png
	fi
	
	install -d \"\${pkgdir}\"/usr/bin
	ln -s /opt/"$name"/"$name" \"\${pkgdir}\"/usr/bin
	chmod -R 755 \"\${pkgdir}\"/opt/"$name"
}" | tee -a "$name"/PKGBUILD

	Build_Arch

	rm -r /tmp/squashfs-root
}

Create_RPM() {
	mkdir -p rpmbuild/RPMS/noarch rpmbuild/SOURCES rpmbuild/SPECS rpmbuild/SRPMS

	echo "Summary: "$description"
Name: "$name"
Version: "$version"
Release: "$version"
License: GPL
URL: https://gitlab.com/julianfairfax/"$name"
Group: System
Packager: "$maintainer_name"
BuildRoot: "$(realpath ./rpmbuild)"

%description
"$description"

%prep
	" | tee rpmbuild/SPECS/"$name".spec
}

Create_Debian() {
	mkdir $name/DEBIAN

	install -d $name/opt/$name
	install -d $name/usr/bin
}

Create_Arch() {
    mkdir "$name"

	mv "${appimage_file##*/}" "$name"/"${appimage_file##*/}"

	echo "pkgname=\"$name\"
pkgver=\"$version\"
arch=(\"$(lscpu | grep Architecture | sed 's/Architecture:.*\ //')\")
pkgdesc=\""$name"\"
pkgrel=\"1\"
source=(\"file://./"${appimage_file##*/}"\")
sha256sums=(\""$sha256sum"\")

prepare() {
	" | tee "$name"/PKGBUILD
}

Build_RPM() {
	cd rpmbuild/SPECS/
	rpmbuild --target noarch -bb "$name".spec

	if [[ "$1" == "install" ]]; then
		cd /tmp/rpmbuild/RPMS/noarch

		dnf install "$name"-"$version"-"$version"-noarch.rpm && rm -r /tmp/rpmbuild
	else
		cp /tmp/rpmbuild/RPMS/noarch/*.rpm "$current_pwd" && rm -r /tmp/"$name"
	fi
}

Build_Debian() {
	ln -s /opt/$name/$name $name/usr/bin

	echo "Package: $name
Version: $version
Architecture: $(dpkg --print-architecture)
Maintainer: $maintainer_name <$maintainer_email>
Description: $description" | tee $name/DEBIAN/control

	dpkg-deb -Z xz -b $name/ .

	if [[ "$1" == "install" ]]; then
		export DEBIAN_FRONTEND=noninteractive

		sudo apt install ./${name}_${version}\_$(dpkg --print-architecture).deb && rm ${name}_${version}\_a*.deb
	else
		cp ${name}_${version}\_a*.deb $current_pwd && rm ${name}_${version}\_a*.deb
	fi
}

Build_Arch() {
	if [[ "$1" == "install" ]]; then
		cd "$name"

		makepkg -si && rm -r /tmp/"$name"
	else
		cp -r "$name" "$current_pwd" && rm -r "$name"
	fi
}

set -x

current_pwd="$PWD"

cd /tmp

if [[ -z "$maintainer_name" ]]; then
	maintainer_name="Julian Fairfax"
fi

if [[ -z "$maintainer_email" ]]; then
	maintainer_email="juliannfairfax@protonmail.com"
fi

if [[ -z "$(which snap)" ]] && [[ ! -f "$1" && ! -f "$2" ]]; then
	if [[ ! -z "$(which dnf)" ]]; then
		sudo dnf install snapd

		sudo ln -s /var/lib/snapd/snap /snap
	fi

	if [[ ! -z "$(which apt)" ]]; then
		export DEBIAN_FRONTEND=noninteractive

		sudo apt install snapd -y
	fi

	if [[ ! -z "$(which pacman)" ]]; then
		sudo pacman -S --noconfirm --needed git base-devel
		git clone https://aur.archlinux.org/snapd.git /tmp/snapd
		cd /tmp/snapd
		makepkg -si
		rm -rf /tmp/snapd

		sudo systemctl enable --now snapd.socket
		sudo ln -s /var/lib/snapd/snap /snap
	fi
fi

if [[ "$3" == "--"* ]]; then
	parameter_3="${3#\-\-}"
else
	parameter_3="$3"
fi

if [[ "$1" == "download" || "$1" == "install" ]]; then
	if [[ ! -z "$2" ]]; then
		snap_id="$(snap info "$2" | grep snap-id | sed 's/.*\:\ //')"
	
		if [[ ! -z "$parameter_3" ]]; then
			snap_build_number="$(snap info "$2" | grep latest/"$parameter_3" | sed 's/.*(//' | sed 's/).*//')"
		else
			snap_build_number="$(snap info "$2" | grep latest/stable | sed 's/.*(//' | sed 's/).*//')"
		fi

		snap_id="${snap_id}_${snap_build_number}"

		if [[ ! -z "$(which dnf)" ]]; then
			Convert_Snap_RPM
		fi

		if [[ ! -z "$(which apt)" ]]; then
			Convert_Snap_Debian
		fi

		if [[ ! -z "$(which pacman)" ]]; then
			Convert_Snap_Arch
		fi
	fi
fi

if [[ ! -z "$1" && -f "$1" ]]; then
	appimage_file="$1"

	if [[ ! -z "$(which dnf)" ]]; then
		Convert_AppImage_RPM
	fi

	if [[ ! -z "$(which apt)" ]]; then
		Convert_AppImage_Debian
	fi

	if [[ ! -z "$(which pacman)" ]]; then
		Convert_AppImage_Arch
	fi
fi

if [[ "$1" == "install" ]]; then
	if [[ ! -z "$2" && -f "$2" ]]; then
		appimage_file="$2"

		if [[ ! -z "$(which dnf)" ]]; then
			Convert_AppImage_RPM
		fi

		if [[ ! -z "$(which apt)" ]]; then
			Convert_AppImage_Debian
		fi

		if [[ ! -z "$(which pacman)" ]]; then
			Convert_AppImage_Arch
		fi
	fi
fi